#!/bin/bash
SOURCE_DIR=/Users/trangnguyen/Desktop/src/kynaforkids-mobile-app/assets/images/quiz/refresh/
DEST_DIR=/Users/trangnguyen/Desktop/src/kynaforkids-mobile-app/assets/images/quiz
WEBP_QUALITY=100
mkdir -p $DEST_DIR
cd $SOURCE_DIR
for f in $(find . -type f -name "*");
do
  fileName="${f%????}"
  fileExtension="${f##*.}"
  fileDir=$(dirname "${f}")
  mkdir -p "$DEST_DIR/$fileDir"
  if [ "$fileExtension" == 'png' ] || [ "$fileExtension" == 'jpg' ]
  then
    echo "Convert $f to WebP"
    cwebp -q $WEBP_QUALITY "$SOURCE_DIR/$f" -o "$DEST_DIR/$fileName.webp"
  else
    echo "Copy $f"
    cp "$SOURCE_DIR/$f" "$DEST_DIR/$f"
  fi
done
