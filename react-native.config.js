module.exports = {
  project: {
    ios: {},
    android: {},
  },
  assets: ['./assets/fonts/', './assets/audio/'],
  dependencies: {
    '@react-native-community/audio-toolkit': {
      platforms: {
        android: null,
      },
    },
    'react-native-code-push': {
      platforms: {
        android: null,
      },
    },
    'react-native-ksyvideo': {
      platforms: {
        ios: null,
      },
    },
    'react-native-splash-screen': {
      platforms: {
        ios: null,
      },
    },
    'react-native-bootsplash': {
      platforms: {
        android: null,
      },
    },
  },
}
