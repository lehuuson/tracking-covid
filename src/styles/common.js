import { StyleSheet, StatusBar, Platform } from 'react-native'

import colors from './colors'

export default StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: Platform.select({ ios: 0, android: StatusBar.currentHeight }),
  },
  separator: {
    borderStyle: 'solid',
    borderRightColor: colors.white,
    borderRightWidth: 2,
  },
  lineDashed: {
    borderColor: '#dddddd',
    borderWidth: .5,
    borderStyle: 'dashed'
  },
  oval: {
    width: 5,
    height: 5,
    backgroundColor: '#333333',
    borderRadius: 2
  }
})
