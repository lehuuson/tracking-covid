export default {
  primaryRegular: 'SVN-ProductSans',
  primaryItalic: 'SVN-ProductSansItalic',
  primaryBoldItalic: 'SVN-ProductSansBoldItalic',
  primaryBold: 'SVN-ProductSansBold',
  primarySemiBold: 'SVN-ProductSansBold',
}
