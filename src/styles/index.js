import {
  Typography,
  Colors,
  Assets,
  ThemeManager,
  Constants,
} from 'react-native-ui-lib'
import { Dimensions, Platform, PixelRatio } from 'react-native'

import { isTablet } from 'react-native-device-info'
import colors from './colors'
import fonts from './fonts'
import commonStyles from './common'
import { isPortrait } from '../utils'

// Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 375
const guidelineBaseWidthLandscape = 667
const guidelineBaseWidthTablet = 960
const guidelineBaseWidthPortraitTablet = 773

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

let safeAreaSize = 0
if (Constants.isIphoneX) {
  const safeAreaInsets = Constants.getSafeAreaInsets()
  safeAreaSize = safeAreaInsets.left + safeAreaInsets.right
}

const widthWindowLandscape = windowWidth > windowHeight ? windowWidth : windowHeight
const heightLandscape = windowWidth > windowHeight ? windowHeight : windowWidth

const widthLandscape = widthWindowLandscape - safeAreaSize

const widthPortrait = windowWidth < windowHeight ? windowWidth : windowHeight

const scalePortrait = (sizePhone, sizeTablet) => {
  if (isTablet() && sizeTablet) {
    return Math.round(
      PixelRatio.roundToNearestPixel(
      (widthPortrait / guidelineBaseWidthPortraitTablet) * (sizeTablet || sizePhone)
    ))
  }
  return Math.round(
    PixelRatio.roundToNearestPixel((widthPortrait / guidelineBaseWidth) * sizePhone))
}

const scaleLandscape = (size, sizeTablet) => {
  const widthMobile = guidelineBaseWidthLandscape
  if (isTablet() && sizeTablet) {
    return PixelRatio.roundToNearestPixel((widthLandscape / guidelineBaseWidthTablet) * (sizeTablet || size))
  }
  return Math.round(
    PixelRatio.roundToNearestPixel((widthLandscape / widthMobile) * size),
  )
}

const normalize = (sizeMobile, sizeTablet) => {
  let safeAreaWidthPadding = 0
  if (Constants.isIphoneX) {
    const safeAreaInsets = Constants.getSafeAreaInsets()
    safeAreaWidthPadding = safeAreaInsets.left + safeAreaInsets.right
  }
  const dimension = Dimensions.get('window')
  const { width } = dimension
  const usableWidth = width - safeAreaWidthPadding
  const baseWidth = isPortrait()
    ? guidelineBaseWidth
    : guidelineBaseWidthLandscape
  let inputSize = isTablet() ? sizeTablet || sizeMobile : sizeMobile
  if (isTablet() && Platform.OS !== 'ios') {
    inputSize -= 2
  }
  const normalizeSize = (usableWidth / baseWidth) * inputSize
  return Math.round(PixelRatio.roundToNearestPixel(normalizeSize))
}

const normalizeLandscape = (sizeMobile, sizeTablet) => {
  let safeAreaWidthPadding = 0
  if (Constants.isIphoneX) {
    safeAreaWidthPadding = 44 + 44
  }
  const dimension = Dimensions.get('window')
  const { width, height } = dimension
  // eslint-disable-next-line no-shadow
  const widthLandscape = width > height ? width : height
  const usableWidth = widthLandscape - safeAreaWidthPadding
  const baseWidth = guidelineBaseWidthLandscape
  let inputSize = isTablet() ? sizeTablet || sizeMobile : sizeMobile
  if (isTablet() && Platform.OS !== 'ios') {
    inputSize -= 2
  }
  const normalizeSize = (usableWidth / baseWidth) * inputSize
  return Math.round(PixelRatio.roundToNearestPixel(normalizeSize))
}

const normalizeVertical = (sizeMobile, sizeTablet) => {
  let safeAreaWidthPadding = 0
  if (Constants.isIphoneX) {
    const safeAreaInsets = Constants.getSafeAreaInsets()
    safeAreaWidthPadding = safeAreaInsets.top + safeAreaInsets.bottom
  }
  const dimension = Dimensions.get('window')
  const { height } = dimension
  const usableHeight = height - safeAreaWidthPadding
  const baseHeight = isPortrait()
    ? guidelineBaseWidthLandscape
    : guidelineBaseWidth
  const inputSize = isTablet() ? sizeTablet || sizeMobile : sizeMobile
  const normalizeSize = (usableHeight / baseHeight) * inputSize
  return Math.round(PixelRatio.roundToNearestPixel(normalizeSize))
}

const fullWidth = widthLandscape

Colors.loadColors(colors)

// guideline height for standard 5" device screen is 680
// eslint-disable-next-line no-unused-vars
const fontScale = (fontSize, standardScreenHeight = 680) => {
  const heightPercent = (fontSize * widthLandscape) / standardScreenHeight
  return Math.round(heightPercent)
}

const loadTypographies = () => {
  Typography.loadTypographies({
    title: {
      fontSize: normalize(35),
      ...Platform.select({
        ios: {
          fontFamily: fonts.primaryRegular,
          fontWeight: 'bold',
        },
        android: {
          fontFamily: fonts.primaryBold,
        },
      }),
    },
    h1: {
      fontSize: normalize(34),
      ...Platform.select({
        ios: {
          fontFamily: fonts.primaryRegular,
          fontWeight: 'bold',
        },
        android: {
          fontFamily: fonts.primaryBold,
        },
      }),
    },
    h2: {
      fontSize: normalize(18),
      lineHeight: 22,
      fontFamily: fonts.primaryBold,
    },
    h3: {
      fontSize: normalize(16),
      lineHeight: 22,
      ...Platform.select({
        ios: {
          fontFamily: fonts.primaryRegular,
          fontWeight: 'bold',
        },
        android: {
          fontFamily: fonts.primaryBold,
        },
      }),
    },
    h4: {
      fontSize: normalize(14),
      lineHeight: 22,
      fontFamily: fonts.primary,
    },
    p: {
      fontSize: normalize(14),
      lineHeight: 20,
      ...Platform.select({
        ios: {
          fontFamily: fonts.primaryRegular,
          fontWeight: '400',
        },
        android: {
          fontFamily: fonts.primaryAndroid,
        },
      }),
    },
    default: {
      fontFamily: fonts.primaryRegular,
      fontSize: normalize(18),
    },
    defaultMedium: {
      fontFamily: fonts.primaryMedium,
      fontSize: normalize(18),
    },
    defaultLight: {
      fontFamily: fonts.primaryRegular,
    },
    text: {
      fontSize: normalize(14),
    },
    textBold: {
      ...Platform.select({
        ios: {
          fontFamily: fonts.primaryRegular,
          fontWeight: 'bold',
        },
        android: {
          fontFamily: fonts.primaryBold,
        },
      }),
      fontSize: normalize(14),
    },
    textSmall: {
      fontSize: normalize(12),
    },
    // font small
    fontS: {
      fontSize: normalize(12, 10),
      fontFamily: fonts.primaryRegular,
    },
    // font normal
    fontN: {
      fontSize: normalize(14, 12),
      fontFamily: fonts.primaryRegular,
    },
    // font medium
    fontM: {
      fontSize: normalize(16, 14),
      fontFamily: fonts.primaryRegular,
    },
    // font large
    fontL: {
      fontSize: normalize(18, 16),
      fontFamily: fonts.primaryRegular,
    },
    // font super large
    fontXL: {
      fontSize: normalize(20, 18),
      fontFamily: fonts.primaryRegular,
    },
    // font super large
    fontXXL: {
      fontSize: normalize(22, 20),
      fontFamily: fonts.primaryRegular,
    },
  })
}

loadTypographies()
Dimensions.addEventListener('change', () => {
  loadTypographies()
})

Assets.loadAssetsGroup('images', {})

Assets.loadAssetsGroup('icons', {})

ThemeManager.setComponentTheme('Dialog', {
  supportedOrientations: [
    'portrait',
    'landscape',
    'landscape-left',
    'landscape-right',
  ],
})

export {
  colors,
  fonts,
  scalePortrait,
  scaleLandscape,
  normalize,
  normalizeLandscape,
  normalizeVertical,
  commonStyles,
  fullWidth,
  widthLandscape,
  widthPortrait,
  heightLandscape
}
