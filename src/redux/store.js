import { applyMiddleware, createStore, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from '@react-native-community/async-storage'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import reducer from './reducer'
import Reactotron from '../../reactotron-config'

const enhancers = [
  applyMiddleware(
    thunkMiddleware,
    createLogger({
      collapsed: true,
      predicate: () => __DEV__,
    }),
  ),
]

const composeEnhancers =
  (__DEV__ &&
    typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose

const enhancer = (__DEV__ ? composeEnhancers(compose(...enhancers, Reactotron.createEnhancer())) : composeEnhancers(...enhancers))

const persistConfig = {
  key: 'root',
  storage,
  blacklist: [
    'modal',
    'loader',
    'game',
    'quiz',
    'learningMap',
    'profile',
    'changePass',
    'speech',
    'dictionary',
    'register',
    'parentSupportRoom',
    'parentNotification',
    'getStart',
    'reportOneByOne',
    'practiceOneByOne',
    'report'
  ],
}

const persistedReducer = persistReducer(persistConfig, reducer)
export const store = createStore(persistedReducer, {}, enhancer)
export const persistor = persistStore(store)
