import { combineReducers } from 'redux'

// Reducer Imports

import app, { RESET_STORE } from '../modules/AppState'

const appReducer = combineReducers({
  app,
})

const rootReducer = (state, action) => {
  // when a reset action is dispatched it will reset redux state
  let data = state
  if (action.type === RESET_STORE) {
    // eslint-disable-next-line no-param-reassign
    const lang = state.language
    const stateGetStart = state.getStart
    data = undefined
    data = {
      language: lang,
      getStart: stateGetStart,
    }
  }
  return appReducer(data, action)
}

export default rootReducer
