
export const PROGRAM_TYPE = {
  VIP: 1,
  SPEAK: 2,
  DEMO: 3,
  TUTORIALS: 4,
  CLASS: 5,
  JUNIOR: 6
}

export const STATUS_ID = {
  DRAFT: 1,
  NEW: 2,
  WAITING: 3,
  APPROVED: 4,
  COMPLETED: 5,
  REJECTED: 6,
  CANCELED: 7,
  DELETED: 8
}

export const ACTIVITY_TYPE = {
  QUIZ: 'quiz',
  SPEECH: 'speech'
}