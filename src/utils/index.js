/* eslint-disable no-useless-escape */
import { Dimensions } from 'react-native'
import projectConfig from '../../project.config'


export function getFullUrlPhotos(suffix) {
  if(!suffix) return ''
  let suffixUrl = suffix
  if(suffixUrl.charAt(0) === '/') suffixUrl = suffixUrl.substring(1)
  return `${projectConfig.mediaBaseUrl}${suffixUrl}`
}

export function isPortrait() {
  const dim = Dimensions.get('screen')
  return dim.height >= dim.width
}

export function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

export function convertCurrencyWithoutUnit(number = 0) {
  try {
    return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  } catch (error) {
    return '0'
  }
}

export function formatPoint(number = 0) {
  try {
    return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  } catch (error) {
    return '0'
  }
}

export function isPhoneNumberVietNam(value) {
  const reg = /(08|09|03|07|05)+([0-9]{8})\b/
  // var reg = /(0)+([0-9]{9})\b/;
  return reg.test(value)
}
