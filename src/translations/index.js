import i18n from 'i18n-js'
import { memoize } from 'lodash'
import en from './en.json'
import vi from './vi.json'
import th from './th.json'

const translations = { en, vi, th }

i18n.defaultLocale = 'en'
i18n.fallbacks = true
i18n.translations = translations

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
)

const setI18nConfig = (lang) => {
  translate.cache.clear()
  i18n.locale = lang
}

// init default language
setI18nConfig('vi')

export {
  setI18nConfig,
  translate
}
