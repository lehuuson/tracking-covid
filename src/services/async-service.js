import AsyncStorage from '@react-native-community/async-storage'
import _ from 'lodash'

const KEY_PERSIST_ROOT = 'persist:root'
const KEY_IS_LOGGED = 'is_logged'
const KEY_NOTIFICATION_READ_IDS = 'notification_read_ids'
const KEY_SHOW_MODAL_UPGRADE_KE = 'show_modal_ke'
export const COUNTRY_CODE = 'country_code'


storeData = async (key, value) => {
  try {
    return await AsyncStorage.setItem(key, JSON.stringify(value))
  } catch (e) {
    //
  }
}

getData = async (key = KEY_PERSIST_ROOT) => {
  try {
    const value = await AsyncStorage.getItem(key)
    if (!_.isEmpty(value)) {
      return JSON.parse(value)
    }
    return null
  } catch (e) {
    //
  }
}

getCurrentChildren = async () => {
  try {
    const value = await getData()
    if (!_.isEmpty(value)) {
      return JSON.parse(value.auth).currentChildren
    }
    return null
  } catch (e) {
    //
  }
}

saveStoreIsLogged = async (isLogged) => {
  try {
    const value = await getData(KEY_IS_LOGGED)
    if (value) {
      return
    }
    storeData(KEY_IS_LOGGED, isLogged)
  } catch (e) {
    //
  }
}

saveNotificationIdRead = async (id, userId) => {
  if (!id || !userId) {
    return
  }
  let readIds = await getData(KEY_NOTIFICATION_READ_IDS)
  const userReadIds = getUserReadIds(readIds, userId)
  userReadIds.push(parseInt(id))
  readIds = { ...readIds, [userId]: userReadIds }
  storeData(KEY_NOTIFICATION_READ_IDS, readIds)
}

getUserReadIds = (readIds, userId) => {
  if (_.isEmpty(readIds) || _.isEmpty(readIds[userId])) {
    return []
  }
  return readIds[userId]
}

isExistNotificationIdRead = async (id, userId) => {
  const readIds = await getData(KEY_NOTIFICATION_READ_IDS)
  const userReadIds = getUserReadIds(readIds, userId)
  if (_.isEmpty(userReadIds)) {
    return false
  }
  return userReadIds.includes(parseInt(id))
}

refreshShowedModalUpgradeKe = async () => {
  try {
    await storeData(KEY_SHOW_MODAL_UPGRADE_KE, null )
  } catch (e) {
    console.log(e.message)
  }
}

// add other navigation functions that you need and export them
const AsyncServices = {
  storeData,
  getData,
  getCurrentChildren,
  KEY_PERSIST_ROOT,
  saveStoreIsLogged,
  KEY_IS_LOGGED,
  KEY_NOTIFICATION_READ_IDS,
  saveNotificationIdRead,
  isExistNotificationIdRead,
  KEY_SHOW_MODAL_UPGRADE_KE,
  refreshShowedModalUpgradeKe
}
export default AsyncServices
