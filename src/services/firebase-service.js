import messaging from '@react-native-firebase/messaging'
import dynamicLinks from '@react-native-firebase/dynamic-links'
import asyncServices from './async-service'

const dynamicLink = {
  test: 'https://kynaenglish.page.link/test'
}
const APP_NOTIFICATION_TOPIC_USER_NOT_ACCOUNT = 'app_notification_topic_user_not_account'
const APP_NOTIFICATION_TOPIC_REMIND_UPDATE_VERSION = 'app_notification_topic_remind_update_version'

getToken = async () => {
  try {
    const token = await messaging().getToken()
    console.log(token)
    return token
  } catch (error) {
    console.warn(error)
  }
  return null
}

checkPermission = async () => {
  try {
    const enabled = await messaging().hasPermission()
    console.log(enabled)
    return enabled
  } catch (error) {
    console.warn(error)
  }
  return false
}

requestPermission = async () => {
  try {
    const authStatus = await  messaging().requestPermission()
    console.log(authStatus)
    return authStatus
  } catch (error) {
    console.warn(error)
  }
}

listener = () => {
  this.listenerNotification()
  this.setBackgroundMessageHandler()
  this.listenerNotificationOpenedListener()
}

clearListener = () => {
  if (this.notificationListener) {
    this.notificationListener()
  }
  if (this.listenerSetBackgroundMessageHandler) {
    this.listenerSetBackgroundMessageHandler()
  }
  if (this.notificationOpenedListener) {
    this.notificationOpenedListener()
  }
}

clearListenerDynamicLink = () => {
  if (this.ltnDynamicLink) {
    this.ltnDynamicLink()
  }
}

listenerNotification = (callback) => {
  /*
    * Triggered when a particular notification has been received in foreground
  * */
  try {
    this.notificationListener = messaging().onMessage((notification) => {
      console.log('particular ', notification)
      callback(notification)
    })
  } catch (err) {
    console.warn(err)
  }
}


setBackgroundMessageHandler = (callback) => {
  /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
  try {
    this.listenerSetBackgroundMessageHandler = messaging().setBackgroundMessageHandler(async notificationOpen => {
      console.log('background ', notificationOpen)
      callback(notificationOpen)
    })

  } catch (err) {
    console.warn(err)
  }
}


listenerNotificationOpenedListener = (callback) => {
  /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
  try {
    this.notificationOpenedListener = messaging().onNotificationOpenedApp((notificationOpen) => {
      console.log('onNotificationOpenedApp ', notificationOpen)
      callback(notificationOpen)
    })
    return notificationOpenedListener
  } catch (err) {
    console.warn(err)
  }
}

getInitialNotification = async () => {
  /*
  * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
  * */
  try {
    const notificationOpen = await messaging().getInitialNotification()
    console.log('getInitialNotification', notificationOpen)
    return notificationOpen
  } catch (err) {
    console.warn(err)
  }
}

subscribeToTopic = (topicName) => {
  try {
    messaging().subscribeToTopic(topicName)
  } catch (err) {
    console.warn(err)
  }
}

unSubscribeFromTopic = (topicName) => {
  try {
    messaging().unsubscribeFromTopic(topicName)
  } catch (err) {
    console.warn(err)
  }
}

listenerDynamicLink = (callback) => {
  try {
    this.ltnDynamicLink = dynamicLinks().onLink((link) => {
      console.log('listenerDynamicLink', link)
      callback(link)
    })
  } catch (err) {
    console.warn(err)
  }
}


getInitialLink = async () => {
  try {
    const link = await dynamicLinks().getInitialLink()
    console.log('listenerBackgroundDynamicLink', link)
    return link
  } catch (err) {
    console.warn(err)
  }
  return null
}

subAndUnSubTopicUserNotAcc = async () => {
  const isLogged = await asyncServices.getData(asyncServices.KEY_IS_LOGGED)
  if (isLogged) {
    return this.unSubscribeFromTopic(APP_NOTIFICATION_TOPIC_USER_NOT_ACCOUNT)
  }
  return this.subscribeToTopic(APP_NOTIFICATION_TOPIC_USER_NOT_ACCOUNT)
}

export default FirebaseService = {
  listener,
  setBackgroundMessageHandler,
  getToken,
  checkPermission,
  requestPermission,
  listenerNotificationOpenedListener,
  getInitialNotification,
  listenerNotification,
  subscribeToTopic,
  unSubscribeFromTopic,
  clearListener,
  listenerDynamicLink,
  clearListenerDynamicLink,
  getInitialLink,
  dynamicLink,
  APP_NOTIFICATION_TOPIC_USER_NOT_ACCOUNT,
  APP_NOTIFICATION_TOPIC_REMIND_UPDATE_VERSION,
  subAndUnSubTopicUserNotAcc,
}