import isEmpty from 'lodash/isEmpty'
import { getModel, isTablet, getReadableVersion, getSystemName } from 'react-native-device-info'
import projectConfig from '../../project.config'
import { store } from '../redux/store'
import { cache, isCacheAble } from '../modules/apiCache/ApiCacheState'
import { getData } from '../selectors/apiSelector'
import { translate } from '../translations/index'

const API_TIMEOUT = 30000

const DEVICE_INFO = {
  mobile_name: getModel(),
  mobile_type: isTablet() ? 'tablet' : 'phone',
  mobile_os: `${getSystemName()}-${getReadableVersion()}`,
  app_version: projectConfig.VERSION,
}

function getQueryString () {
  const queryData = {
    ...DEVICE_INFO,
  }
  return Object.entries(queryData).map(([key, value]) => `${key}=${value}`).join('&')
}

export default {
  post,
  get,
  postForm,
}

function timeout(ms, promise) {
  return new Promise((resolve, reject) => {
    setTimeout(() => reject(new Error('Server did not respond')), ms)
    return promise.then(resolve, reject)
  })
}

async function get(url, data, version = 'v1') {
  let apiPath = url
  if (data && !isEmpty(data)) {
    apiPath += `?${Object.entries(data)
      .map(([key, value]) => `${key}=${value}`)
      .join('&')}`
  }
  const requestOptions = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  }
  const apiUrl = `${projectConfig.apiBaseUrl + version + apiPath + (data && !isEmpty(data) ? '&' : '?')}${getQueryString()}`
  const apiCacheAble = isCacheAble(url)
  if (apiCacheAble) {
    const cacheData = getData(apiPath)(store.getState())
    if (cacheData) {
      store.dispatch(cache(url, cacheData))
    }
  }
  return timeout(
    API_TIMEOUT,
    fetch(apiUrl, requestOptions)
      .then(response => {
        if (!response.ok) {
          return response.json().then(error => Promise.reject(error))
        }
        return response.json()
      })
      .then(
        responseJson => {
          if (responseJson.success === true) {
            // store cache
            if (apiCacheAble) {
              store.dispatch(cache(url, responseJson.data))
              if (url !== apiPath) {
                store.dispatch(cache(apiPath, responseJson.data))
              }
            }
          }
          return responseJson
        },
        error => {
          if (error.success === false && error.data.status === 401) {
            const { message } = error.data
            handleLogout(message)
          }
          return error
        },
      )
      .catch(error => ({ success: false, data: [{ message: error.message }] })),
  )
}

async function postForm(url, formData, version = 'v1') {
  const apiUrl = `${projectConfig.apiBaseUrl + version + url}?${getQueryString()}`
  const requestOptions = {
    method: 'POST',
    headers: {
    },
    body: formData,
  }
  return timeout(
    API_TIMEOUT,
    fetch(apiUrl, requestOptions)
      .then(response => {
        if (!response.ok) {
          return response.json().then(error => Promise.reject(error))
        }
        return response.json()
      })
      .then(
        responseJson => responseJson,
        error => {
          if (error.success === false && error.data.status === 401) {
            const { message } = error.data
            handleLogout(message)
          }
          return error
        },
      )
      .catch(error => ({ success: false, data: [{ message: error.message }] })),
  )
}

async function post(url, data = {}, version = 'v1') {
  const body = JSON.stringify(data)
  const apiUrl = `${projectConfig.apiBaseUrl + version + url}?${getQueryString()}`
  const requestOptions = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body,
  }
  return timeout(
    API_TIMEOUT,
    fetch(apiUrl, requestOptions)
      .then(response => {
        if (!response.ok) {
          return response.json().then(error => Promise.reject(error))
        }
        return response.json()
      })
      .then(
        responseJson => responseJson,
        error => {
          if (error.success === false && error.data.status === 401) {
            const { message } = error.data
            handleLogout(message)
          }
          return error
        },
      )
      .catch(error => ({ success: false, data: [{ message: error.message }] })),
  )
}


function handleLogout(message) {
  const authState = store.getState().auth
  if (!authState || !authState.isLoggedIn || !authState.user) {
    return false
  }
  store.dispatch(logout())
  showAlertNotification({ title: translate('Common.titleDanger'), message: message || translate('Common.messageLoginAgain') })
}
