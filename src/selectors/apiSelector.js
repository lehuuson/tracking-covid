export const getData = (cacheKey) => (state) => state.apiCache[cacheKey]

export const hasCache = (cacheKey) => (state) => !!state.apiCache[cacheKey]
