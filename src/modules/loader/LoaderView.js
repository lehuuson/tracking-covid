import React from 'react'
import { ActivityIndicator, StyleSheet } from 'react-native'
import { View, Text, Colors } from 'react-native-ui-lib'

export default class Loader extends React.PureComponent {
  render() {
    const { size, style, textStyle, color, active, text } = this.props
    if (!active) {
      return null
    }
    return (
      <View style={[styles.overlayContainer, style]}>
        <View flex center>
          <ActivityIndicator
            animating={active}
            size={size}
            color={color}
          />
          {text &&
          <Text textBold style={[styles.textStyle, textStyle]}>{text}</Text>
          }
        </View>
      </View>
    )
  }
}

Loader.defaultProps = {
  size: 'large',
  color: Colors.gray,
  active: false,
  text: null,
}

const styles = StyleSheet.create({
  overlayContainer: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: Colors.rgba(Colors.white, 0.7),
    zIndex: 100,
  },
  textStyle: {
    color: Colors.dark10,
    marginTop: 10,
    fontSize: 16,
  },
})
