import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import {
  LEVEL,
} from './NavigationState'
import NavigationServices from '../../services/navigation-service'
import LevelScreen from '../level/LevelContainer'


const Stack = createStackNavigator()

export default function RootNavigation({ onStateChange, initialRouteName }) {
  const fadeAnimation = ({ current }) => ({
    cardStyle: {
      opacity: current.progress
    }
  })
  return (
    <NavigationContainer
      onStateChange={onStateChange}
      ref={NavigationServices.setTopLevelNavigator}
    >
      <Stack.Navigator
        initialRouteName={initialRouteName}
        headerMode='none'
      >
        <Stack.Screen name={LEVEL} component={LevelScreen} options={{ gestureEnabled: false, animationEnabled: false  }} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
