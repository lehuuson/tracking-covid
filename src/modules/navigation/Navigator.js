import React from 'react'
import AppNavigator from './RootNavigation'

export default function NavigatorView({ onStateChange, initialRouteName }) {
  return (
    <AppNavigator
      initialRouteName={initialRouteName}
      onStateChange={onStateChange}
    />
  )
}
