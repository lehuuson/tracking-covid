import React from 'react'

import SplashScreen from 'react-native-splash-screen'
import RNBootSplash from 'react-native-bootsplash'
import { AppState, Platform, StatusBar } from 'react-native'
import Navigator from './navigation/Navigator'
import  { getActiveRouteName } from '../services/navigation-service'
import * as SCREEN_NAMES from './navigation/NavigationState'

export default class AppView extends React.Component {

  static propTypes = {
  }

  constructor(props) {
    super(props)
    this.state = {
      currentRouteName: '',
    }
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      SplashScreen.hide()
    } else {
      RNBootSplash.hide()
    }
    AppState.addEventListener('change', this._handleAppStateChange)
  }


  componentWillUnmount() {

  }

  _handleAppStateChange = nextAppState => {
  }


  onStateChange = action => {

  }

  render() {
    const { isSplashScreenFinish } = this.props
    return (
      <>
        <Navigator
          initialRouteName={SCREEN_NAMES.LEVEL}
          onStateChange={this.onStateChange}
          uriPrefix='/kynaenglish'
        />
      </>
    )
  }
}
