// ------------------------------------
// App Global Constant
// ------------------------------------
export const RESET_STORE = 'RESET_STORE'

// ------------------------------------
// Action
// ------------------------------------

// ------------------------------------
// Reducer
// ------------------------------------
const ACTION_HANDLERS = {
}
const initialState = {
}
export default function AppReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
