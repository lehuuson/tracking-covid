// ------------------------------------
// Constant
// ------------------------------------
import { GET_PACKAGES_API } from '../level/LevelState'


const cacheAbleList = {
  GET_PACKAGES_API,
}
export const ACTION_CACHE = 'ApiCacheState/ACTION_CACHE'
export const ACTION_REMOVE_CACHE = 'ApiCacheState/ACTION_REMOVE_CACHE'
export const ACTION_CLEAR_CACHE = 'ApiCacheState/ACTION_CLEAR_CACHE'

// ------------------------------------
// Action
// ------------------------------------
export function cache(cacheKey, data) {
  return (dispatch) => {
    dispatch({ type: ACTION_CACHE, cacheKey, data })
  }
}
export function removeCache(cacheKey) {
  return (dispatch) => {
    dispatch({ type: ACTION_REMOVE_CACHE, cacheKey })
  }
}
export function clearCache() {
  return (dispatch) => {
    dispatch({ type: ACTION_CLEAR_CACHE })
  }
}
export function isCacheAble(cacheKey) {
  const cacheAbleListValues = Object.values(cacheAbleList)
  return !!cacheAbleListValues.includes(cacheKey)
}

// ------------------------------------
// Reducer
// ------------------------------------
const ACTION_HANDLERS = {
  [ACTION_CACHE]: (state, action) => {
    const newState = { ...state }
    newState[action.cacheKey] = action.data
    return {
      ...newState
    }
  },
  [ACTION_REMOVE_CACHE]: (state, action) => {
    const newState = { ...state }
    delete newState[action.cacheKey]
    return {
      ...newState
    }
  },
  [ACTION_CLEAR_CACHE]: () => initialState
}
const initialState = {
}
export default function ApiCacheReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
