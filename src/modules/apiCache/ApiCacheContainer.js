import { compose } from 'recompose'
import { connect } from 'react-redux'

import ApiCacheView from './ApiCacheView'

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ApiCacheView)
