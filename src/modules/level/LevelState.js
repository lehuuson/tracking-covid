/* eslint-disable camelcase */
import { apiService } from '../../services'
import projectConfig from '../../../project.config'

// ------------------------------------
// Constant
// ------------------------------------
export const GET_PACKAGES_API = 'kynaenglish/default/get-packages'

// ------------------------------------
// Action
// ------------------------------------


// ------------------------------------
// Reducer
// ------------------------------------
const ACTION_HANDLERS = {

}
const initialState = {
}
export default function LevelReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
