import { compose } from 'recompose'
import { connect } from 'react-redux'

import LevelView from './LevelView'
import { createLoadingSelector } from '../../selectors/loading-selector'



const loadingSelector = createLoadingSelector([
])

export default compose(
  connect(
    state => {
      return {
      }
    },
    {
    },
  ),
)(LevelView)
