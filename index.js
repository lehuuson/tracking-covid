import { AppRegistry, unstable_enableLogBox, YellowBox } from 'react-native'
import App from './App'
import { name as appName } from './app.json'

unstable_enableLogBox()
YellowBox.ignoreWarnings([
  'Animated: `useNativeDriver` was not specified',
  'componentWillMount',
  'getNode',
  'Animated.event now requires a second argument for options',
  'Non-serializable values were found in the navigation state',
  'Can\'t perform a React state update on an unmounted component'
])

AppRegistry.registerComponent(appName, () => App)
