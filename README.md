## Getting Started

#### 1. Clone and Install

```bash
# Clone the repo
git clone git@bitbucket.org:kynavn/kynaforkids-mobile-app.git

# Navigate to clonned folder and Install dependencies
cd kynaforkids-mobile-app && yarn install

# Install Pods
cd ios && pod install
```

#### 2. Open app in your iOS simulator

Run this command to start the development server and to start your app on iOS simulator:
```
yarn run:ios
```

Or, if you prefer Android:
```
yarn run:android
```

#### 3. Coding Standard
```
# Config eslint
yarn lint

# Fix code
yarn lint:fix
```

#### 3. Generator Module
```
# Install plop
yarn global add plop

# Generate new module
plop module <module_name>

# Add navigation
src/modules/navigation/RootNavigation.js
```
