import { Platform } from 'react-native'

const ENV = 'production'
const VERSION = '1.3.2'

let apiBaseUrl = ''
let mediaBaseUrl = ''
let codePushKey = Platform.select({
  ios: '9P788mKrF3hs6wZPAEmVy2y53sTdJFOfQVvFd',
  android: 'rZ2DJZL9UmuBaiKePyq_1XJxXUDCITu0mr9Jw'
})
const appleAppID = '1506869739'
const googlePackageName = 'com.kynaenglish'
const iosPackageName = 'com.kynaenglish.app'
switch (ENV) {
  case 'local':
    apiBaseUrl = 'http://api.kynaforkids.local/'
    mediaBaseUrl = 'http://media.kynaforkids.local/'
    break
  case 'development':
    apiBaseUrl = 'https://api-dev.kynaforkids.vn/'
    mediaBaseUrl = 'https://media-dev.kynaforkids.vn'
    break
  case 'staging':
    apiBaseUrl = 'https://api-staging.kynaforkids.vn/'
    mediaBaseUrl = 'https://media-staging.kynaforkids.vn/'
    break
  case 'production':
    apiBaseUrl = 'https://api.kynaforkids.vn/'
    mediaBaseUrl = 'https://cdn.kynaforkids.vn/'
    codePushKey = Platform.select({
      ios: '_EOHRJKuhQ_rNun2Msk3BB1wgFFFLo3kvOELP',
      android: 'sLvIFAhBrfeQgmYNvzE5F-RnsJ5KRNnA8K8KJ'
    })
    break
  default:
    break
}

module.exports = {
  apiBaseUrl,
  mediaBaseUrl,
  codePushKey,
  ENV,
  VERSION,
  appleAppID,
  googlePackageName,
  iosPackageName
}

