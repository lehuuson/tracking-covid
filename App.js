import 'react-native-gesture-handler'
import React from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import { enableScreens } from 'react-native-screens'
import { store, persistor } from './src/redux/store'

import AppView from './src/modules/AppContainer'


enableScreens()

export default class App extends React.PureComponent {

  render() {
    return (
      <Provider store={store}>
        <PersistGate
          persistor={persistor}
        >
          <AppView />
        </PersistGate>
      </Provider>
    )
  }
}
