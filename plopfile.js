module.exports = function(plop) {
  plop.setGenerator('module', {
    description: 'Generates new module with or without redux connection',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Module name (Casing will be modified)'
      },
      {
        type: 'list',
        name: 'type',
        message: 'Choose Module type',
        choices: ['statefull', 'stateless']
      }
    ],
    actions(data) {
      const actions = [
        {
          type: 'add',
          path: 'src/modules/{{camelCase name}}/{{properCase name}}View.js',
          templateFile: 'generators/module/ModuleView.js.hbs'
        },
        {
          type: 'add',
          path:
            'src/modules/{{camelCase name}}/{{properCase name}}Container.js',
          templateFile: 'generators/module/ModuleContainer.js.hbs'
        }
      ]

      if (data.type === 'statefull') {
        actions.push({
          type: 'add',
          path: 'src/modules/{{camelCase name}}/{{properCase name}}State.js',
          templateFile: 'generators/module/ModuleState.js.hbs'
        })
        actions.push({
          type: 'modify',
          path: 'src/redux/reducer.js',
          pattern: /\/\/ Reducer Imports/gi,
          template:
            "// Reducer Imports\r\nimport {{camelCase name}} from '../modules/{{camelCase name}}/{{properCase name}}State'"
        })
        actions.push({
          type: 'modify',
          path: 'src/redux/reducer.js',
          pattern: /\/\/ Reducers/gi,
          template: '// Reducers\r\n  {{camelCase name}},'
        })
      }

      return actions
    }
  })
}
